#include <iostream>
#include <string>
#include <stdexcept>
#include <cstdlib>

typedef struct PersonalData2 {
    std::string surname;
    std::string firstname = "John";
} PersonalDataType2;

int compare_PersonalData2(PersonalData2* person1, PersonalData2* person2) {
    int diff = person1->surname.compare(person2->surname);
    if (diff != 0) return diff;
    return person1->firstname.compare(person2->firstname);
}

int compare_PersonalData2(PersonalData2 person1, PersonalData2 person2) {
    return compare_PersonalData2(&person1, &person2);
}

int compare_int(int data1, int data2) {
    return data1 - data2;
}

std::string int_to_string(int value) {
    return std::to_string(value);
}

std::string PersonalData2ToString(PersonalData2* person) {
    return person->surname + "(" + person->firstname + ")";
}

std::string GetRandomString(int length) {
    std::string text = "";
    int charLowerSign = 97;
    int charUpperSign = 122;
    for (int i = 0; i < length; ++i) {
        text += (char)((rand() % (charUpperSign - charLowerSign + 1)) + charLowerSign);
    }
    return text;
}

PersonalData2* GetRandomPerson() {
    PersonalData2* person = new(PersonalData2);
    person->surname = GetRandomString(2);
    person->firstname = GetRandomString(1);
    return person;
}

//////////////////////////////////
template <typename T> class RBT {
private:
    typedef struct Item {
        T data;
        Item* left = NULL;
        Item* right = NULL;
        Item* parent = NULL;
        bool isRed;
        int id;
    };
    Item* root = NULL;
    int size = 0;
    int lastId = -1;

    void rotLeft(Item* child, Item* parent) {
        if (child->left != NULL) child->left->parent = parent;
        parent->right = child->left;

        child->parent = parent->parent;
        if (parent->parent == NULL) root = child;
        else if (parent == parent->parent->left) parent->parent->left = child;
        else parent->parent->right = child;

        parent->parent = child;
        child->left = parent;
    }

    void rotRight(Item* child, Item* parent) {
        if (child->right != NULL) child->right->parent = parent;
        parent->left = child->right;

        child->parent = parent->parent;
        if (parent->parent == NULL) root = child;
        else if (parent == parent->parent->left) parent->parent->left = child;
        else parent->parent->right = child;

        parent->parent = child;
        child->right = parent;
    }

    Item* getUncle(Item* item) {
        Item* parent = item->parent;
        Item* grandparent = parent->parent;
        if (grandparent == NULL) return NULL;
        Item* uncle;
        if (parent == grandparent->left) uncle = grandparent->right;
        else uncle = grandparent->left;
        return uncle;
    }

    bool checkIfRed(Item* item) {
        if (item == NULL) return false;
        return item->isRed;
    }

    std::string idToString(Item* element) {
        if (element == NULL) return "NULL";
        return std::to_string(element->id);
    }
    std::string colorToString(Item* element) {
        if (element == NULL || !element->isRed) return "BLACK";
        return "RED";
    }
    std::string itemsToString(Item* item, std::string separator, std::string(*dataToString)(T)) {
        return "    (" + idToString(item) + ": [" + colorToString(item) + ", p: " + idToString(item->parent) + ", l: " + idToString(item->left) + ", r: " + idToString(item->right) + "], data: " + dataToString(item->data) + ")" + separator + "\n";
    }
    int calculateHeight(Item* item) {
        if (item == NULL)
            return 0;
        else {
            int lHeight = calculateHeight(item->left);
            int rHeight = calculateHeight(item->right);

            if (lHeight > rHeight) return lHeight + 1;
            return rHeight + 1;
        }
    }
    void removeItem(Item* item) {
        if (item->left != NULL) removeItem(item->left);
        if (item->right != NULL) removeItem(item->right);
        delete item;
    }
public:
    int Size() {
        return size;
    }
    int CalcHeight() {
        return calculateHeight(root);
    }
    void fixOrderAndColors(Item* item) {
        while (item != root && checkIfRed(item->parent)) {
            Item* uncle = getUncle(item);
            Item* parent = item->parent;
            Item* grandparent = parent->parent;
            if (parent == grandparent->left) {
                if (checkIfRed(uncle)) {
                    parent->isRed = false;
                    uncle->isRed = false;
                    grandparent->isRed = true;
                    item = grandparent;
                }
                else {
                    if (item == parent->right) {
                        rotLeft(item, item->parent);
                        item = parent;
                        parent = item->parent;
                        grandparent = parent->parent;
                    }
                    parent->isRed = false;
                    grandparent->isRed = true;
                    rotRight(parent, grandparent);
                }
            }
            else {
                if (checkIfRed(uncle)) {
                    parent->isRed = false;
                    uncle->isRed = false;
                    grandparent->isRed = true;
                    item = grandparent;
                }
                else {
                    if (item == parent->left) {
                        rotRight(item, item->parent);
                        item = parent;
                        parent = item->parent;
                        grandparent = parent->parent;
                    }
                    parent->isRed = false;
                    grandparent->isRed = true;
                    rotLeft(parent, grandparent);
                }
            }
        }
        root->isRed = false;
    }
    void AddItem(T* item, int (*data_cmp)(T, T)) {
        Item* newItem = new Item();
        newItem->data = *item;
        newItem->id = ++lastId;
        newItem->isRed = true;
        if (root == NULL) {
            root = newItem;
            newItem->isRed = false;
        }
        else {
            Item* temp = root;
            Item* tempParent = NULL;
            bool addLeft = false;
            while (temp != NULL) {
                tempParent = temp;
                addLeft = false;
                if (data_cmp(*item, temp->data) <= 0) {
                    temp = temp->left;
                    addLeft = true;
                }
                else temp = temp->right;
            }
            newItem->parent = tempParent;
            if (addLeft) {
                tempParent->left = newItem;
            }
            else tempParent->right = newItem;
        }
        fixOrderAndColors(newItem);
        ++size;
    }
    void AddItem(T item, int (*data_cmp)(T, T)) {
        T* tempData = new T();
        *tempData = item;
        AddItem(tempData, data_cmp);
    }

    Item* FindItem(T dataToFind, int (*data_cmp)(T, T)) {
        if (root == NULL) return NULL;
        Item* temp = root;
        while (temp != NULL && data_cmp(dataToFind, temp->data) != 0) {
            if (data_cmp(dataToFind, temp->data) < 0) {
                temp = temp->left;
            }
            else temp = temp->right;
        }
        return temp;
    }

    void PreOrder() {
        return PreOrder(root);
    }
    void PreOrder(Item* item) {
        if (item != NULL) {
            std::cout << item->data << " ";
            PreOrder(item->left);
            PreOrder(item->right);
        }
    }
    void PreOrder(Item* item, std::string& text, std::string(*dataToString)(T)) {
        if (item != NULL) {
            text += itemsToString(item, ",", dataToString);
            PreOrder(item->left, text, dataToString);
            PreOrder(item->right, text, dataToString);
        }
    }

    void InOrder() {
        return InOrder(root);
    }
    void InOrder(Item* item) {
        if (item != NULL) {
            InOrder(item->left);
            std::cout << item->data << " ";
            InOrder(item->right);
        }
    }

    std::string ToString(std::string(*dataToString)(T)) {
        std::string finalText = "red black tree:\n  size: " + std::to_string(size) + "\n  height: " + std::to_string(CalcHeight()) + "\n  {\n";
        PreOrder(root, finalText, dataToString);
        finalText += "  }";
        return finalText;
    }

    void Clear() {
        removeItem(root);
        root = NULL;
        size = 0;
        lastId = -1;
    }
};
/////////////////////////////////

int main()
{
    RBT<int>* tree1 = new RBT<int>();
    tree1->AddItem(55, compare_int);
    tree1->AddItem(69, compare_int);
    tree1->AddItem(62, compare_int);
    tree1->AddItem(57, compare_int);
    tree1->AddItem(35, compare_int);
    tree1->AddItem(83, compare_int);
    tree1->AddItem(72, compare_int);
    tree1->AddItem(74, compare_int);
    std::cout << "Preorder:" << std::endl;
    tree1->PreOrder();
    std::cout << std::endl << "InOrder:" << std::endl;
    tree1->InOrder();
    std::cout << std::endl << std::endl;
    std::cout << tree1->ToString(int_to_string) << std::endl;
    tree1->Clear();
    std::cout << std::endl << std::endl << "#AFTER CLEAR#" << std::endl;
    std::cout << tree1->ToString(int_to_string) << std::endl << std::endl << std::endl;

    RBT<PersonalData2*>* tree2 = new RBT<PersonalData2*>();
    PersonalData2* person1 = new PersonalData2;
    person1->surname = "asd";
    PersonalData2* person2 = new PersonalData2;
    person2->surname = "wad";
    PersonalData2* person3 = new PersonalData2;
    person3->surname = "poo";
    tree2->AddItem(person1, compare_PersonalData2);
    tree2->AddItem(person2, compare_PersonalData2);
    tree2->AddItem(person3, compare_PersonalData2);
    std::cout << tree2->ToString(PersonalData2ToString) << std::endl;
    return 0;
}
